import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadisticasCostoBeneficioComponent } from './estadisticas-costo-beneficio.component';

describe('EstadisticasCostoBeneficioComponent', () => {
  let component: EstadisticasCostoBeneficioComponent;
  let fixture: ComponentFixture<EstadisticasCostoBeneficioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadisticasCostoBeneficioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadisticasCostoBeneficioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
