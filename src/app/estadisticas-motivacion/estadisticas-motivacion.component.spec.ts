import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadisticasMotivacionComponent } from './estadisticas-motivacion.component';

describe('EstadisticasMotivacionComponent', () => {
  let component: EstadisticasMotivacionComponent;
  let fixture: ComponentFixture<EstadisticasMotivacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadisticasMotivacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadisticasMotivacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
