import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { DatosEmpresaComponent } from './datos-empresa/datos-empresa.component';
import { CarrouselComponent } from './carrousel/carrousel.component';
import { FooterComponent } from './footer/footer.component';

import { RegistroComponent } from './registro/registro.component';
import { LetrasComponent } from './letras/letras.component';
import { CityComponent } from './city/city.component';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DashComponent } from './dash/dash.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { AnunciosComponent } from './anuncios/anuncios.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { SoporteComponent } from './soporte/soporte.component';
import { NavbarService } from './navbar.service';
import { EmpresaDatosCuentaComponent } from './empresa-datos-cuenta/empresa-datos-cuenta.component';
import { EmpresaDatosPlanComponent } from './empresa-datos-plan/empresa-datos-plan.component';
import { EmpresaDatosAdsComponent } from './empresa-datos-ads/empresa-datos-ads.component';
import { EmpresaModificarCuentaComponent } from './empresa-modificar-cuenta/empresa-modificar-cuenta.component';
import { EstadisticasGraficaClicsComponent } from './estadisticas-grafica-clics/estadisticas-grafica-clics.component';
import { EstadisticasMotivacionComponent } from './estadisticas-motivacion/estadisticas-motivacion.component';
import { EstadisticasGeneralesComponent } from './estadisticas-generales/estadisticas-generales.component';
import { CrearPlanCrearAnuncioComponent } from './crear-plan-crear-anuncio/crear-plan-crear-anuncio.component';
import { SoporteCorreoComponent } from './soporte-correo/soporte-correo.component';
import { AnunciosModificaradsComponent } from './anuncios-modificarads/anuncios-modificarads.component';
import { EstadisticasCostoBeneficioComponent } from './estadisticas-costo-beneficio/estadisticas-costo-beneficio.component';
import { MapeoComponent } from './mapeo/mapeo.component';
import { MapaComponent } from './mapa/mapa.component';





@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    DatosEmpresaComponent,
    CarrouselComponent,
    FooterComponent,
    RegistroComponent,
    LetrasComponent,
    CityComponent,
    DashComponent,
    EstadisticasComponent,
   
    AnunciosComponent,
    EmpresaComponent,
    SoporteComponent,
    EmpresaDatosCuentaComponent,
    EmpresaDatosPlanComponent,
    EmpresaDatosAdsComponent,
    EmpresaModificarCuentaComponent,
    EstadisticasGraficaClicsComponent,
    EstadisticasMotivacionComponent,
    EstadisticasGeneralesComponent,

    CrearPlanCrearAnuncioComponent,
    SoporteCorreoComponent,
    
    AnunciosModificaradsComponent,
    EstadisticasCostoBeneficioComponent,
    MapeoComponent,
    MapaComponent,
   


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    NavbarService 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
