import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresaDatosPlanComponent } from './empresa-datos-plan.component';

describe('EmpresaDatosPlanComponent', () => {
  let component: EmpresaDatosPlanComponent;
  let fixture: ComponentFixture<EmpresaDatosPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaDatosPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresaDatosPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
