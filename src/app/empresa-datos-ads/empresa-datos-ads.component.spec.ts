import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresaDatosAdsComponent } from './empresa-datos-ads.component';

describe('EmpresaDatosAdsComponent', () => {
  let component: EmpresaDatosAdsComponent;
  let fixture: ComponentFixture<EmpresaDatosAdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaDatosAdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresaDatosAdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
