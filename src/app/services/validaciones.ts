import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';

  
@Injectable({
  providedIn: 'root'
})
export class Validaciones{
    
  constructor(private http: HttpClient){}
 // Validacion numeros
  public validarNumeros(cadena : any){
     var  i: number = 0;
     var comprueba : number = 0;
     var comprueba2 : number = 0;
      for(i=0 ; i<cadena.length ; i++){
        if(cadena.slice(i,i+1)==='1'||cadena.slice(i,i+1)==='2'||cadena.slice(i,i+1)==='3'||cadena.slice(i,i+1)==='4'||cadena.slice(i,i+1)==='5'||cadena.slice(i,i+1)==='6'||cadena.slice(i,i+1)==='7'||cadena.slice(i,i+1)==='8'||cadena.slice(i,i+1)==='9'||cadena.slice(i,i+1)==='0'||cadena.slice(i,i+1)==='+'||cadena.slice(i,i+1)===' '){
            comprueba++;
           }
           else{
            comprueba--;
           }
      }
      if(comprueba === i){
        return "si";
      }
      if(comprueba !== i){
        return "El parametro : " +cadena +" debe ser numerico";
      }
    
  }
  // Validacion Telefonos
  public validarTelefonos(cadena : any){
    var  i: number = 0;
    var comprueba : number = 0;
    var comprueba2 : number = 0;
     for(i=0 ; i<cadena.length ; i++){
       if(cadena.slice(i,i+1)==='1'||cadena.slice(i,i+1)==='2'||cadena.slice(i,i+1)==='3'||cadena.slice(i,i+1)==='4'||cadena.slice(i,i+1)==='5'||cadena.slice(i,i+1)==='6'||cadena.slice(i,i+1)==='7'||cadena.slice(i,i+1)==='8'||cadena.slice(i,i+1)==='9'||cadena.slice(i,i+1)==='0'||cadena.slice(i,i+1)==='+'||cadena.slice(i,i+1)===' '){
           comprueba++;
          }
          else{
           comprueba--;
          }
        
        
        }
       
     if(comprueba === i && comprueba > 10){
       return "si";
     }
     // validacion para agregar la lada mexicana automaticamente
     if(comprueba === 10){
      return "mexico";
    }
     else{
      return "El parametro : " +cadena +" le faltan numeros";
      }
  }
  // Validacion campo vacio
  
 public validacionVacio(cadena:any){
  if(cadena === null || cadena === undefined || cadena === ""){
    return "debes llenar todos los parametros para continuar";
  }
  else{
    return "si";
  }
 }
 }


