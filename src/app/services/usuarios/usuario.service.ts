import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
// import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
  
@Injectable({
  providedIn: 'root'
})
export class UsuarioService{
  API_URL = 'http://localhost:3000'

  constructor(private http: HttpClient){}

  getAllUsers(){
    return this.http.get(`${this.API_URL}/users`);
  }
  deleteUsers( id: number) {
    return this.http.delete(`${this.API_URL}/users/${id}`);
  }
  createUser( usuario: any) {
    console.log(usuario);
    return this.http.post(`${this.API_URL}/users`, usuario);
  }

  getUsuarioByUserNameAndPassword(login: any) {
    console.log(login);
    return this.http.post(`${this.API_URL}/login`, login);
  }


}




