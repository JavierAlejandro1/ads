import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresaModificarCuentaComponent } from './empresa-modificar-cuenta.component';

describe('EmpresaModificarCuentaComponent', () => {
  let component: EmpresaModificarCuentaComponent;
  let fixture: ComponentFixture<EmpresaModificarCuentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaModificarCuentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresaModificarCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
