import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoporteCorreoComponent } from './soporte-correo.component';

describe('SoporteCorreoComponent', () => {
  let component: SoporteCorreoComponent;
  let fixture: ComponentFixture<SoporteCorreoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoporteCorreoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoporteCorreoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
