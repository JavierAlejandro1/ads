import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadisticasGraficaClicsComponent } from './estadisticas-grafica-clics.component';

describe('EstadisticasGraficaClicsComponent', () => {
  let component: EstadisticasGraficaClicsComponent;
  let fixture: ComponentFixture<EstadisticasGraficaClicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadisticasGraficaClicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadisticasGraficaClicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
