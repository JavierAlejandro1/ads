import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresaDatosCuentaComponent } from './empresa-datos-cuenta.component';

describe('EmpresaDatosCuentaComponent', () => {
  let component: EmpresaDatosCuentaComponent;
  let fixture: ComponentFixture<EmpresaDatosCuentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaDatosCuentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresaDatosCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
