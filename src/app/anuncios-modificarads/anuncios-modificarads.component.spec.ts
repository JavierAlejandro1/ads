import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnunciosModificaradsComponent } from './anuncios-modificarads.component';

describe('AnunciosModificaradsComponent', () => {
  let component: AnunciosModificaradsComponent;
  let fixture: ComponentFixture<AnunciosModificaradsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnunciosModificaradsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnunciosModificaradsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
