import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearPlanCrearAnuncioComponent } from './crear-plan-crear-anuncio.component';

describe('CrearPlanCrearAnuncioComponent', () => {
  let component: CrearPlanCrearAnuncioComponent;
  let fixture: ComponentFixture<CrearPlanCrearAnuncioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearPlanCrearAnuncioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearPlanCrearAnuncioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
