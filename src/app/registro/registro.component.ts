import { Component, OnInit } from '@angular/core';
import {UsuarioService} from '../services/usuarios/usuario.service';
import {Validaciones } from '../services/validaciones';
import { Router } from '@angular/router'; 
import { NavbarService } from '../navbar.service';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit{

  Usuario: any = {};

  constructor(private usuarioServices: UsuarioService,
    private validacion : Validaciones,
    public nav: NavbarService ) {
    }
 
 guardarUsuario(){
   // Validaciones de telefono, numericas y de campos vacios
console.log(this.Usuario.nombre);
console.log(this.Usuario.email);
console.log(this.Usuario.password);
  if(
    this.validacion.validacionVacio(this.Usuario.nombre)==="si"&&
    this.validacion.validacionVacio(this.Usuario.email)==="si"&&
    this.validacion.validacionVacio(this.Usuario.password)==="si"
    ){
         this.usuarioServices.createUser(this.Usuario)
   .subscribe((data: any) => {
     console.log(data);
   });
  
  
  }
   else{
       // Mensajes de error 
       alert("Todos los campos deben estar llenos");
       }
}

iniciarSesion(){
  alert("sesion iniciada");
  //document.getElementById("navBar").style.visibility="hidden";    
  
}
ngOnInit() {
  this.nav.show();
  
 }
}


  

