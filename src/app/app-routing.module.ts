import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { RegistroComponent } from './registro/registro.component';
import { DashComponent } from './dash/dash.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { AnunciosComponent } from './anuncios/anuncios.component';
import { SoporteComponent } from './soporte/soporte.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { EmpresaDatosCuentaComponent } from './empresa-datos-cuenta/empresa-datos-cuenta.component';
import { EmpresaDatosPlanComponent } from './empresa-datos-plan/empresa-datos-plan.component';
import { EmpresaDatosAdsComponent } from './empresa-datos-ads/empresa-datos-ads.component';
import { EmpresaModificarCuentaComponent } from './empresa-modificar-cuenta/empresa-modificar-cuenta.component';
import { EstadisticasGraficaClicsComponent } from './estadisticas-grafica-clics/estadisticas-grafica-clics.component';
import { EstadisticasMotivacionComponent } from './estadisticas-motivacion/estadisticas-motivacion.component';
import { EstadisticasGeneralesComponent } from './estadisticas-generales/estadisticas-generales.component';
import { CrearPlanCrearAnuncioComponent } from './crear-plan-crear-anuncio/crear-plan-crear-anuncio.component';
import { AnunciosModificaradsComponent } from './anuncios-modificarads/anuncios-modificarads.component';
import { SoporteCorreoComponent } from './soporte-correo/soporte-correo.component';
import { EstadisticasCostoBeneficioComponent } from './estadisticas-costo-beneficio/estadisticas-costo-beneficio.component';
import { DatosEmpresaComponent} from './datos-empresa/datos-empresa.component';
import { MapeoComponent } from './mapeo/mapeo.component';
import { MapaComponent } from './mapa/mapa.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'registro' ,component: RegistroComponent},
  {path: 'dash' , component: DashComponent,
  children: [
  
    {path: '' ,component: EmpresaComponent, children: [
      {path: '' ,component: DatosEmpresaComponent},
      {path: 'empresaDatosPlan' ,component: EmpresaDatosPlanComponent},
      {path: 'empresaDatosAds' ,component: EmpresaDatosAdsComponent},
      {path: 'empresaModificarCuenta' ,component: EmpresaModificarCuentaComponent}

    ]},
    {path: 'estadisticas', component: EstadisticasComponent, children:[
      {path: '' ,component: EstadisticasGraficaClicsComponent },
      {path: 'motivacion' ,component: EstadisticasMotivacionComponent},
      {path: 'generales' ,component: EstadisticasGeneralesComponent},
      {path: 'costoBeneficio' ,component: EstadisticasCostoBeneficioComponent}

    ]},
    {path: 'mapa', component: MapaComponent, children:[
      {path: '' ,component: MapeoComponent },
     

    ]},
    
    {path: 'anuncios', component: AnunciosComponent, children:[
      {path: '' , component: CrearPlanCrearAnuncioComponent  },
      {path: 'modificarAnuncio' ,component: AnunciosModificaradsComponent }
     

    ]},
    {path: 'soporte', component: SoporteComponent, children:[
      {path: '' ,component: SoporteCorreoComponent }
  

    ]}

  ]},
  
  {path: 'anuncios' ,component: AnunciosComponent},
  {path: 'soporte' ,component: SoporteComponent},
  {path: 'estadisticas' ,component: EstadisticasComponent}
  
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
